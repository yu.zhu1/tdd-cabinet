STORY 1
AC1
1. Given a cabinet, when I put the pag into this cab, I should get a ticket.
2. Given a cabinet, when I put nothing into this cab, I should get an error message "Please put something into this cabinet".

AC2
1. Given a cabinet and a corresponded ticket, I should get the bag that I saved.
2. Given a cabinet and no ticket, I should get an error message "Please insert a ticket".
3. Given a cabinet and a wrong ticket, I should get an error message "Pleas insert a right ticket".

STORY 2
AC1
1. Given a cabinet and some empty lockers left, I should be able to save the bag and get a ticket.
2. Given a cabinet and no empty lockers left, I should get a notification: "Insufficient empty lockers".

STORY 3
AC1
1. Given a cabinet and big lockers, I should be able to save the bag and get a ticket when I choose the big locker.
2. Given a cabinet and big lockers, When I did not choose any locker size, I should get an error message "Please select a locker size".

STORY 4
AC1
1. Given a cabinet, when I choose to save the big bag into the big locker, I should be able to get a ticket.
2. Given a cabinet, when I choose to save the medium/small/mini bag into the big locker, I should be able to get a ticket.
3. Given a cabinet, when I choose to save the huge bag into the big locker, I should get an error message "Unable to save a huge bag into the big locker".
4. Given a cabinet, when I choose to save the medium bag into the medium locker, I should be able to get a ticket.
5. Given a cabinet, when I choose to save the small/mini bag into the medium locker, I should be able to get a ticket.
6. Given a cabinet, when I choose to save the huge bag into the medium locker, I should get an error message "Unable to save a huge bag into the medium locker".
6. Given a cabinet, when I choose to save the big bag into the medium locker, I should get an error message "Unable to save a big bag into the medium locker".

AC2
1. Given a cabinet and no empty big lockers left, when I choose to save the big bag into the big locker, I should get an error message "".
2. Given a cabinet and no empty big lockers left, when I choose to save the medium bag into the medium locker, I should be able to get a ticket.