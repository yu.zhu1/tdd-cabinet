package com.twuc.bagSaving;

public enum LockerSize {
    BIG(4),
    MEDIUM(3);

    public int getNumber() {
        return number;
    }

    private int number;
    LockerSize(int number) {
        this.number = number;
    }
}
