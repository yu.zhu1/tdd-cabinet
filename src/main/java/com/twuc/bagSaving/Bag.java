package com.twuc.bagSaving;

public class Bag {

    public BagSize getBagSize() {
        return bagSize;
    }

    public Bag(BagSize bagSize) {
        this.bagSize = bagSize;
    }

    private BagSize bagSize;

}
