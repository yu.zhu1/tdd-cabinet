package com.twuc.bagSaving;

public enum BagSize {
    HUGE(5),
    BIG(4),
    MEDIUM(3),
    SMALL(2),
    MINI(1);

    private int number;

    public int getNumber() {
        return number;
    }

    BagSize(int number) {
        this.number = number;
    }
}
