package com.twuc.bagSaving;

public class Cabinet {


    public Cabinet(int bigCapacity, int mediumCapacity ) {
        if (mediumCapacity <= 0) {
            throw new IllegalArgumentException();
        }
        if (bigCapacity <= 0) {
            throw new IllegalArgumentException();
        }

        this.bigLocker = new Locker(bigCapacity);
        this.mediumLocker = new Locker(mediumCapacity);
    }

    private Locker bigLocker;
    private Locker mediumLocker;

    public Ticket save(Bag bag, LockerSize lockerSize) {
        if (bag == null) {
            throw new IllegalArgumentException("Please put something into this cabinet");
        }

        if (lockerSize == null) {
            throw new IllegalArgumentException("Please select a locker size");
        }

        matchBagSizeAndLockerSize(bag, lockerSize);

        Ticket ticket = new Ticket();

        if (lockerSize == LockerSize.BIG) {
            bigLocker.putBagIntoLocker(bag, ticket);
        }

        if (lockerSize == LockerSize.MEDIUM) {
            mediumLocker.putBagIntoLocker(bag, ticket);
        }

        return ticket;
    }


    public Bag get(Ticket ticket) {
        if (ticket == null) {
            throw new IllegalArgumentException("Please insert a ticket");
        }

        if ( mediumLocker.getLocker().containsKey(ticket)) {
            return mediumLocker.getBag(ticket);
        }

        if (bigLocker.getLocker().containsKey(ticket)) {
            return bigLocker.getBag(ticket);
        }

        throw new IllegalArgumentException("Please insert a right ticket");

    }


    private void matchBagSizeAndLockerSize(Bag bag, LockerSize lockerSize) {
        if (bag.getBagSize().getNumber() > lockerSize.getNumber()) {
            throw new RuntimeException(String.format("Unable to save a %s bag into the %s locker", bag.getBagSize(), lockerSize));
        }
    }

}


