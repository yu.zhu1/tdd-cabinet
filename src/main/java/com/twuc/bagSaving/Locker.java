package com.twuc.bagSaving;

import java.util.HashMap;

class Locker {
    private int capacity;

    HashMap<Ticket, Bag> getLocker() {
        return locker;
    }

    private HashMap<Ticket, Bag> locker = new HashMap<>();


    Locker(int capacity) {
        this.capacity = capacity;
    }

    void putBagIntoLocker(Bag bag, Ticket ticket) {
        if (locker.size() >= capacity) {
            throw new RuntimeException("Insufficient empty lockers");
        }
        locker.put(ticket, bag);
    }

    Bag getBag(Ticket ticket) {
        return locker.remove(ticket);
    }




}
