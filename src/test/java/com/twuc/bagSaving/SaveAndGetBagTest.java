package com.twuc.bagSaving;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SaveAndGetBagTest {
    @Test
    void should_get_a_ticket_when_save_the_bag() {
        Bag bag = new Bag(BagSize.BIG);
        Cabinet cabinet = new Cabinet(Integer.MAX_VALUE, Integer.MAX_VALUE);
        Ticket ticket = cabinet.save(bag, LockerSize.BIG);
        assertNotNull(ticket);
    }

    @Test
    void should_get_an_error_message_when_save_nothing() {
        Cabinet cabinet = new Cabinet(Integer.MAX_VALUE, Integer.MAX_VALUE);
        String message = assertThrows(IllegalArgumentException.class, () -> {
            cabinet.save(null, LockerSize.BIG);
        }).getMessage();
        assertEquals("Please put something into this cabinet",message);
    }

    @Test
    void should_get_the_bag_when_insert_the_corresponded_ticket() {
        Cabinet cabinet = new Cabinet(Integer.MAX_VALUE, Integer.MAX_VALUE);
        Bag bag = new Bag(BagSize.BIG);
        Ticket ticket = cabinet.save(bag, LockerSize.BIG);

        Bag anotherBag = cabinet.get(ticket);
        assertNotNull(anotherBag);
        assertSame(bag, anotherBag);
    }

    @Test
    void should_get_error_message_when_insert_no_ticket() {
        Cabinet cabinet = new Cabinet(Integer.MAX_VALUE, Integer.MAX_VALUE);
        Bag bag = new Bag(BagSize.BIG);
        cabinet.save(bag, LockerSize.BIG);
        String message = assertThrows(IllegalArgumentException.class, () -> {
            cabinet.get(null);
        }).getMessage();
        assertEquals("Please insert a ticket", message);
    }

    @Test
    void should_get_error_message_when_insert_wrong_ticket() {
        Cabinet cabinet = new Cabinet(Integer.MAX_VALUE, Integer.MAX_VALUE);
        Bag bag = new Bag(BagSize.BIG);
        cabinet.save(bag, LockerSize.BIG);
        Ticket wrongTicket = new Ticket();
        String message = assertThrows(IllegalArgumentException.class, () -> {
            cabinet.get(wrongTicket);
        }).getMessage();
        assertEquals("Please insert a right ticket", message);
    }



}



