package com.twuc.bagSaving;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;

public class LockerSizeTest {
    private static Object[][] createBiggerBagAndSmallerLockerArguments() {
        return new Object[][] {
                new Object[] { BagSize.BIG, LockerSize.MEDIUM },
                new Object[] { BagSize.HUGE, LockerSize.MEDIUM},
                new Object[] { BagSize.HUGE, LockerSize.BIG},
        };
    }

   private static Object[][] createSmallerBagAndBiggerLockerArgument() {
        return new Object[][] {
                new Object[] { BagSize.BIG, LockerSize.BIG},
                new Object[] { BagSize.MEDIUM, LockerSize.BIG},
                new Object[] { BagSize.SMALL, LockerSize.BIG},
                new Object[] { BagSize.MINI,  LockerSize.BIG},
                new Object[] { BagSize.MEDIUM, LockerSize.MEDIUM},
                new Object[] { BagSize.SMALL, LockerSize.MEDIUM},
                new Object[] { BagSize.MINI, LockerSize.MEDIUM}
        };
   }

    @Test
    void should_get_a_ticket_when_choose_the_big_lockers() {
        Cabinet cabinet = new Cabinet(Integer.MAX_VALUE, Integer.MAX_VALUE);
        Bag bag = new Bag(BagSize.BIG);
        Ticket ticket = cabinet.save(bag, LockerSize.BIG);
        assertNotNull(ticket);
    }

    @Test
    void should_get_error_message_when_choose_no_locker_size() {
        Cabinet cabinet = new Cabinet(Integer.MAX_VALUE, Integer.MAX_VALUE);
        Bag bag = new Bag(BagSize.BIG);
        String message = assertThrows(IllegalArgumentException.class, () -> {
            cabinet.save(bag, null);
        }).getMessage();
        assertEquals("Please select a locker size", message);
    }

    @ParameterizedTest
    @MethodSource("createBiggerBagAndSmallerLockerArguments")
    void should_throw_error_message_when_save_bigger_bag_into_the_smaller_locker(BagSize bagSize, LockerSize lockerSize) {
        Cabinet cabinet = new Cabinet(Integer.MAX_VALUE, Integer.MAX_VALUE);
        Bag bag = new Bag(bagSize);
        String message = assertThrows(RuntimeException.class, () -> {
            cabinet.save(bag, lockerSize);
        }).getMessage();
        assertEquals(String.format("Unable to save a %s bag into the %s locker", bagSize, lockerSize), message);
    }

    @ParameterizedTest
    @MethodSource("createSmallerBagAndBiggerLockerArgument")
    void should_get_ticket_when_save_smaller_bag_into_the_bigger_locker(BagSize bagSize, LockerSize lockerSize) {
        Cabinet cabinet = new Cabinet(Integer.MAX_VALUE, Integer.MAX_VALUE);
        Bag bag = new Bag(bagSize);
        Ticket ticket = cabinet.save(bag, lockerSize);
        assertNotNull(ticket);
    }

    @Test
    void should_get_error_message_when_save_big_bag_into_big_locker_when_no_empty_big_lockers_left() {
        Cabinet cabinet = new Cabinet(1, Integer.MAX_VALUE);
        Bag bag = new Bag(BagSize.BIG);
        cabinet.save(bag, LockerSize.BIG);
        assertThrows(RuntimeException.class, ()-> {
            cabinet.save(bag, LockerSize.BIG);
        });
    }


    @Test
    void should_get_ticket_when_save_medium_bag_into_medium_locker_when_no_empty_big_lockers_left() {
        Cabinet cabinet = new Cabinet(1, Integer.MAX_VALUE);
        Bag bigBag = new Bag(BagSize.BIG);
        cabinet.save(bigBag, LockerSize.BIG);
        Bag mediumBag = new Bag(BagSize.MEDIUM);
        Ticket ticket = cabinet.save(mediumBag, LockerSize.MEDIUM);
        assertNotNull(ticket);
    }
}
