package com.twuc.bagSaving;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

public class CapacityTest {

    @Test
    void should_get_the_ticket_when_there_are_some_empty_lockers_left() {
        Cabinet cabinet = new Cabinet(1, 1);
        Bag bag = new Bag(BagSize.BIG);
        Ticket ticket = cabinet.save(bag, LockerSize.BIG);
        assertNotNull(ticket);
    }

    @Test
    void should_get_notification_when_no_empty_lockers_left() {
        Cabinet cabinet = new Cabinet(1, 1);
        Bag bag = new Bag(BagSize.BIG);
        cabinet.save(bag, LockerSize.BIG);
        Bag anotherBag = new Bag(BagSize.BIG);
        String message = assertThrows(RuntimeException.class, () -> {
            cabinet.save(anotherBag, LockerSize.BIG);
        }).getMessage();
        assertEquals("Insufficient empty lockers", message);
    }


    @ParameterizedTest
    @ValueSource(ints = {-1, 0})
    void should_get_error_when_capacity_is_negative(int ints) {
        assertThrows(IllegalArgumentException.class, () -> new Cabinet(ints, ints));
    }

}
